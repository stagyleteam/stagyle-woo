<?php if ( have_posts() ) : ?>

<div class="container main-content blog">

	<div class="row" id="blog" >
    
	<?php if ( ( alhenalite_template('sidebar') == "left-sidebar" ) || ( alhenalite_template('sidebar') == "right-sidebar" ) ) : ?>
        
        <div class="<?php echo alhenalite_template('span') .' '. alhenalite_template('sidebar'); ?>"> 
        
        <div class="row"> 
        
    <?php endif; ?>
        
		<?php while ( have_posts() ) : the_post(); ?>

        <div <?php post_class(); ?> >
    
				<?php do_action('alhenalite_postformat'); ?>
        
                <div style="clear:both"></div>
            
            </div>
		
		<?php endwhile; else:  ?>

        <div class="container">
           
            <div class="row" id="blog" >

                <div class="post-container col-md-12">
        
                    <div class="post-article">

                        <h1>Not found</h1>

                        <p><?php _e( 'Sorry, no posts matched your criteria',"nevada-lite" ) ?> </p>
         
                    </div>
        
                </div>
                
            </div>
            
        </div>
	
		<?php endif; ?>
        
	<?php if ( ( alhenalite_template('sidebar') == "left-sidebar" ) || ( alhenalite_template('sidebar') == "right-sidebar" ) ) : ?>
        
        </div>
	
    </div>
        
    <?php endif; ?>

	<?php get_sidebar();?>

    </div>
    
</div>