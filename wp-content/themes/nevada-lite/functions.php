<?php

/*-----------------------------------------------------------------------------------*/
/* UNREGISTER SIDEBAR */
/*-----------------------------------------------------------------------------------*/   

if (!function_exists('nevada_lite_unregister_sidebar')) {

	function nevada_lite_unregister_sidebar(){
		unregister_sidebar( 'home_sidebar_area' );
		unregister_sidebar( 'category_sidebar_area' );
	}
	
	add_action( 'widgets_init', 'nevada_lite_unregister_sidebar', 11 );
	
}

/*-----------------------------------------------------------------------------------*/
/* WIDGETS WITHOUT PADDING */
/*-----------------------------------------------------------------------------------*/   

if (!function_exists('nevada_lite_widget_class')) {

	function nevada_lite_widget_class( $params ) {
		
		$name = $params[0]['widget_name'];
		$id = $params[0]['id'];

		/*-----------------------------------------------------------------------------------*/
		/* SIDE SIDEBAR AREA */
		/*-----------------------------------------------------------------------------------*/   

		if ( in_array( $id, array(
			"side_sidebar_area")
			))
			$params[0]['before_widget'] = preg_replace( '/class="widget-box/', 'class="post-article', $params[0]['before_widget'], 1 );

		return $params;
		
	}

	add_filter( 'dynamic_sidebar_params', 'nevada_lite_widget_class' );

}

/*-----------------------------------------------------------------------------------*/
/* STYLES AND SCRIPTS */
/*-----------------------------------------------------------------------------------*/ 

if (!function_exists('nevada_lite_scripts_styles')) {

	function nevada_lite_scripts_styles() {

		wp_deregister_style ( 'alhenalite-style' );
		wp_enqueue_style( 'nevada-lite-style', get_stylesheet_directory_uri() . '/style.css' );
		
		if ( get_theme_mod('wip_skin') ) :

			wp_deregister_style ( 'alhenalite ' . get_theme_mod('wip_skin') );
			wp_enqueue_style( 'nevada-lite-' . get_theme_mod('wip_skin') , get_stylesheet_directory_uri() . '/skins/' . get_theme_mod('wip_skin') . '.css' ); 

		else:
		
			wp_enqueue_style( 'nevada-lite-light_blue', get_stylesheet_directory_uri() . '/skins/light_blue.css' ); 

		endif;

		wp_deregister_style ( 'google-fonts' );
		wp_enqueue_style( 'nevada-lite-google-fonts', 'https://fonts.googleapis.com/css?family=Marck+Script|Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=latin-ext,vietnamese' );
		
	}
	
	add_action( 'wp_enqueue_scripts', 'nevada_lite_scripts_styles', 12 );

}

/*-----------------------------------------------------------------------------------*/
/* SETUP */
/*-----------------------------------------------------------------------------------*/ 

if (!function_exists('nevada_lite_setup')) {

	function nevada_lite_setup() {

		load_child_theme_textdomain( 'nevada-lite', get_stylesheet_directory() . '/languages' );

	}

	add_action( 'after_setup_theme', 'nevada_lite_setup', 11 );

}

?>