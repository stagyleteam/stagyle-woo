<div class="container">
    <h2><?php echo esc_html($fudge_lite_registration_title); ?></h2>
    <p><span><?php echo esc_html($fudge_lite_registration_tagline); ?></span></p>
    <p><?php echo esc_html($fudge_lite_registration_text); ?></p>
    <div class="eventbrite"><?php echo do_shortcode($fudge_lite_registration_eventbrite); ?></div>
</div>