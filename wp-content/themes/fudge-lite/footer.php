<footer>
    <div class="container">
        <h2><?php bloginfo('name'); ?></h2>
        <p><?php echo esc_html(get_theme_mod('footer', '')); ?></p>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
