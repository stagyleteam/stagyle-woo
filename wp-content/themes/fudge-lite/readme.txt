=== Fudge Lite ===
Contributors: showthemes
Requires at least: WordPress 4.4
Tested up to: WordPress 4.7
Version: 1.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, grid-layout, custom-colors, custom-header, custom-menu, custom-logo, featured-images, full-width-template, theme-options, translation-ready, e-commerce, education, entertainment

== Description ==
Fudge Lite is a responsive event WordPress theme. This theme is perfect for industry professionals with a need to showcase one or many events using a stunning one page layout.
Unlike typical themes where the events are secondary to the blog or another post type, Fudge Lite showcases your events and puts them front and center.
This colorful theme includes 10 wonderful color skins to choose from so you can find a palette that fits your needs.
The theme also include built-in events schedule, location (with Google map), speaker showcase with pop-up information section, gallery, blog, and social icons.

* Top notch schedule and performer management
* Performers Profiles
* Clean Session Display
* Fully Responsive
* Easy Maps and POIs
* Easy Media display
* Social Icons
* Contact Form
* 10 preset color palettes

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Fudge Lite in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.
5. Navigate to Pages > Add New in your admin panel and create a new page. Then, in the Page Attributes box in the right side of the page, select the Home option in the Template selectbox.
6. Navigate to Settings > Reading in your admin panel. Set the "Front page displays" field to "A static page" and select the page created in the previous point in the "Front Page" field. After that click the "Save Changes" button.

MENUS
Due to the limited space available for the menu section, only 1 level of menus is managed in Fudge and only a fixed amounts of menu items are allowed to be added to the menu (around 5).

== Copyright ==

Fudge Lite WordPress Theme, Copyright 2017 Showthemes LLC
Fudge Lite is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Fudge Lite WordPress Theme bundles the following third-party resources:

Genericons icon font, Copyright 2013 Automattic
Genericons are licensed under the terms of the GNU GPL, Version 2 (or later)
Source: http://www.genericons.com

jQuery UI theme, Copyright 2013 jQuery Foundation and other contributors
jQuery UI is licensed under the terms of the MIT license
Source: http://jqueryui.com/themeroller/

TGM-Plugin-Activation package, Copyright 2011 Thomas Griffin
TGM-Plugin-Activation is licensed under the terms of GNU GPL, Version 2 (or later)

Modernizr library, Copyright 2016
Modernizer is licensed under the terms of MIT & BSD

jQuery Placeholder library, Copyright Mathias Bynens
jQuery Placeholder is licensed under the terms of the GNU GPL, Version 2 (or later)

Images in the package are licensed under the terms of the Creative Commons Zero (CC0)

== Changelog ==

= 1.0 =
* Initial release

= 1.1 =
* Floats properly cleared after the content area.
* The post date changed to consider the user choice of date format.
* Comment section improvements: added comment author, added link to comment author website, added comment anchor and removed list numbers in comments.
* Set body default background color.
* Added visual fixes for default menu.
* Added visual fixes for long titles.
* Added page links in static pages.
* Changed name for home sidebar.
* Fixed gaps in blog page.

= 1.2 =
* Added support for custom background color.
* Added support for add_editor_style.
* Fixed design for default WP widgets.
* Removed id on search form.

= 1.3 =
* Removed useless empty parameters in customizer items.
* Added unminified version of modernizr-2.5.0.min.js in the package.
* Removed some useless images in the package.
* Updated readme.txt

= 1.4 =
* Notice about recommended plugin is now dismissable.
* Added unminified version of jquery-ui-1.10.3.custom.min in the package.
* Unminified some portions of css files.
* Escaped some texts attributes in comments.php
* Added missing traslation functions in widget-contact.php and functions-customizer.php
* Removed social widget and added customized styles to allow users to user default Custom Menu widget instead.
* Fixed mobile menu position
* Removed overriding of globals