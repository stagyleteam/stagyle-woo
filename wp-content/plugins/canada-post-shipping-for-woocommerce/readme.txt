=== Canada Post Shipping For WooCommerce ===
Contributors: canship
Tags: shipping, canada post, woocommerce
Requires at least: 3.0.1
Tested up to: 4.7.4
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Add Canada Post as a shipping option for your customers

== Description ==

Canada Post Shipping For WooCommerce is a plugin that adds support to WooCommerce for Canada Post. 

After installing the plugin your customers will have the option to choose Canada Post as a shipping method 
during the checkout. The plugin automatically calculates both letter and parcel shipping rates for your customers 
by using the weights you provide on each of your inventory items.

If you need help with the plugin please feel free to email at mike@nositesleft.com.

More detailed instructions and help documents can be found on the 
[NoSitesLeft - WooCommerce Plugins and Support](http://www.nositesleft.com/support) website.

== Installation ==

1. Download the plugin and activate it
2. Go to WooCommerce > Settings > Shipping
3. Enable the Canada Post shipping method
4. Enter your postal code if you want to enable parcel shipping

== Screenshots ==

1. Settings Page
2. Shipping Quotes

== Changelog ==

= 2.1.0 =
Added new settings fields for people to use their own API keys to avoid potential issues with performance and reliability.

= 2.0.3 =
Fixed a PHP 7 warning

= 2.0.2 =
Fixed a bug where we forgot include_once and the classes were being included more than once with recent version of WooCommerce

= 2.0.1 =
Fixed a bug with imperial weight conversions breaking the API calls.

= 2.0.0 =
Added the Canada Post API integration to the plugin.

= 1.0.3 =
Test with WordPress 4.4 and update readme.txt.

= 1.0.2 =
Support for multiple weights and dimension units in WooCommerce.

= 1.0.1 =
Modified content on the settings page.

= 1.0.0 =
Initial Release
