<?php

class Canada_Post_Shipping_For_WooCommerce_Canada_Post {
	
	private $rates = array();
	
	private function get_normalized_weight($weight) {
		$woo_weight_unit = strtolower(get_option('woocommerce_weight_unit'));

		if ($woo_weight_unit != 'kg') {
			switch ($woo_weight_unit) {
				case 'g':
					$weight *= 0.001;
					break;
				case 'lbs':
					$weight *= 0.4353;
					break;
				case 'oz':
					$weight *= 0.0283495;
			}
		}

		return $weight;
	}
	
	private function make_request_to_canada_post($request_data) {
		$results = wp_remote_post('https://soa-gw.canadapost.ca/rs/ship/price/', $request_data);
		
		$xml = simplexml_load_string($results['body']);
		$json = json_encode($xml);
		$array = json_decode($json, TRUE);
		
		for ($i = 0; $i < count($array['price-quote']); $i++) {
			$rate = array(
				'id' => $array['price-quote'][$i]['service-code'],
				'label' => $array['price-quote'][$i]['service-name'],
				'cost' => $array['price-quote'][$i]['price-details']['due'],
				'calc_tax' => 'per_item'
			);
			
			array_push($this->rates, $rate);
		}
	}
	
	private function build_canada_post_api_request($weight, $destination) {
		$general_options = get_option('woocommerce_canada_post_shipping_by_nosites_left_settings');
		$origin_postal_code = strtoupper(str_replace(' ', '', $general_options['postal_code']));

		$key = base64_encode($general_options['api_username'] . ':' . $general_options['api_password']);
		
		if ($general_options['api_username'] . $general_options['api_password'] == '') {
			$key = 'YWI3MjBjMGRhNzY2M2I5ODo2ZTg1Y2ExOTgxZDVlMzIzZWRiODJi';
		}
		
		$request_data = array(
			'method' => 'POST',
			'timeout' => '15',
			'headers' => array(
				'Accept' => 'application/vnd.cpc.ship.rate-v3+xml',
				'Authorization' => 'Basic ' . $key,
				'Accept-Language' => 'en-CA',
				'Content-Type' => 'application/vnd.cpc.ship.rate-v3+xml'
			),
			'body' => 
				'<mailing-scenario xmlns="http://www.canadapost.ca/ws/ship/rate-v3">
					<quote-type>counter</quote-type>
					<parcel-characteristics>
						<weight>' . round($weight, 2) . '</weight>
					</parcel-characteristics>
					<origin-postal-code>' . $origin_postal_code . '</origin-postal-code>');
					
					if ($destination['country'] == 'CA') {
						$request_data['body'] .=
							'<destination>
								<domestic>
									<postal-code>' . strtoupper(str_replace(' ', '', $destination['postcode'])) . '</postal-code>
								</domestic>
						  </destination>';
					}
					else if ($destination['country'] == 'US') {
						$request_data['body'] .=
							'<destination>
								<united-states>
									<zip-code>' . str_replace(' ', '', $destination['postcode']) . '</zip-code>
								</united-states>
						  </destination>';
					}
					else
					{
						$request_data['body'] .=
							'<destination>
								<international>
									<country-code>' . strtoupper($destination['country']) . '</country-code>
								</international>
						  </destination>';
					}
					
		$request_data['body'] .= '</mailing-scenario>';
		
		return $this->make_request_to_canada_post($request_data);	
	}
	
	private function calculate_letter_rate_shipping($destination, $weight_total) {
		$postage_amount = 0.00;
		
		if ($destination['country'] == 'CA') {
			if ($weight_total < 0.030) {
				$postage_amount = 1.00;
			}
			else if ($weight_total < 0.050) {
				$postage_amount = 1.20;
			}
			else if ($weight_total < 0.100) {
				$postage_amount = 1.80;
			}
			else if ($weight_total < 0.200) {
				$postage_amount = 2.95;
			}
			else if ($weight_total < 0.300) {
				$postage_amount = 4.10;
			}
			else if ($weight_total < 0.400) {
				$postage_amount = 4.70;
			}
				else if ($weight_total <= 0.500) {
				$postage_amount = 5.05;
			}
		}
		else if ($destination['country'] == 'US') {
			if ($weight_total < 0.030) {
				$postage_amount = 1.20;
			}
			else if ($weight_total < 0.050) {
				$postage_amount = 1.80;
			}
			else if ($weight_total < 0.100) {
				$postage_amount = 2.95;
			}
			else if ($weight_total < 0.200) {
				$postage_amount = 5.15;
			}
			else if ($weight_total <= 0.500) {
				$postage_amount = 10.30;
			}
		}
		else {
			if ($weight_total < 0.030) {
				$postage_amount = 2.50;
			}
			else if ($weight_total < 0.050) {
				$postage_amount = 3.60;
			}
			else if ($weight_total < 0.100) {
				$postage_amount = 5.90;
			}
			else if ($weight_total < 0.200) {
				$postage_amount = 10.30;
			}
			else if ($weight_total <= 0.500) {
				$postage_amount = 20.60;
			}
		}
		
		if ($postage_amount > 0.00) {
			$rate = array(
				'id' => 'letter_rate',
				'label' => 'Snail Mail',
				'cost' => $postage_amount,
				'calc_tax' => 'per_item'
			);
			
			array_push($this->rates, $rate);
		}
	}
	
	public function calculate_shipping($package, $destination, $allow_letter_rates) {
		
		$weight_total = 0.00;
		
		foreach($package as $item) {
			$non_normalized_weight = $item['data']->get_weight();
			$weight_total += $item['quantity'] * $this->get_normalized_weight($non_normalized_weight);
		}
		
		if ($allow_letter_rates) {
			$this->calculate_letter_rate_shipping($destination, $weight_total);
		}
		
		$this->build_canada_post_api_request($weight_total, $destination);
		
		return $this->rates;
	}
}

?>