<?php

class Canada_Post_Shipping_For_WooCommerce_Shipping_Method extends WC_Shipping_Method {
	
	public function __construct() {
		$this->id                 = 'canada_post_shipping_by_nosites_left'; 
		$this->method_title       = 'Canada Post';  
		$this->title = 'Canada Post';

		$general_options = get_option('woocommerce_canada_post_shipping_by_nosites_left_settings');
		if (!isset($general_options['api_username']) || !isset($general_options['api_password']) || $general_options['api_username'] == '' || $general_options['api_password'] == '') {
			$this->method_description = '<span style="color: Red">Although the plugin will work as-is we now strongly recommend you set up your own free Canada Post account for the most reliable service and best rates.</span><br /><br /><span style="color: Red"><a href="http://nositesleft.com/how-to-set-up-your-canada-post-account/"><b>Click here for instructions on how to set up your account and configure the plugin</b></a></span>';
		}
		else {
			$this->method_description = 'Integrates with Canada Post allowing you to provide accurate shipping quotes for your customers.'; 
		}
		
		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
		
		$this->init_form_fields();
		$this->init_settings();
	}
	
	public function init_form_fields() {
		$general_options = get_option('woocommerce_canada_post_shipping_by_nosites_left_settings');

		if (!isset($general_options['api_username']) || $general_options['api_username'] == '') {
			$user_name_description = '<span style="color: Red">The Canada Post API User Name.</span>';
		}
		else {
			$user_name_description = 'The Canada Post API User Name';
		}

		if (!isset($general_options['api_password']) || $general_options['api_password'] == '') {
			$password_description = '<span style="color: Red">The Canada Post API Password<span style="color: Red">';
		}
		else
		{
			$password_description = 'The Canada Post API Password';
		}

		$this->form_fields = array(
			'enabled' => array(
				'title' => 'Enable/Disable',
				'label' => 'Enable this option to turn on this shipping method.',
				'default' => 'yes',
				'type' => 'checkbox'),
			'api_username' => array(
				'title' => 'API Username',
				'description' => $user_name_description,
				'default' => '',
				'type' => 'text'
			),
			'api_password' => array(
				'title' => 'API Password',
				'description' => $password_description,
				'default' => '',
				'type' => 'text'
			),
			'allow_stamps' => array(
				'title' => 'Allow Letter Rate Shipping',
				'label' => 'Enable this option to turn on letter rate shipping using stamps.',
				'type' => 'checkbox'),
			'postal_code' => array(
				'title' => 'Postal Code',
				'description' => 'The postal code packages are shipped from. Fill this out if you want to ship parcels.',
				'type' => 'text')
			);
	}
	
	public function calculate_shipping($package = Array()) {
		$allow_letter_rates = $this->get_option('allow_stamps') == 'yes';
		
		include_once 'canada-post-shipping-for-woocommerce-canada-post.php';
		$canada_post = new Canada_Post_Shipping_For_WooCommerce_Canada_Post();
		$rates = $canada_post->calculate_shipping($package["contents"], $package["destination"], $allow_letter_rates);
		
		foreach ($rates as $rate) {
			$this->add_rate($rate);
		}
	}
	
	public function admin_options() {
		
		parent::admin_options();
				
		echo('<h3>Support and Feature Requests</h3>');
		echo('<p>');
		echo("We're here to help! <br /><br />If you need help, have questions or would like a feature added please feel free to reach out.<br /><br />");
		echo("The best way to contact us is through <a href='http://www.nositesleft.com/support'>our website.</a>");
		echo('</p>');
		echo('<br />');
		echo('<h3>Premium Version</h3>');
		echo('<p>');
		echo('We do offer a premium version of our plugin that gives you more control over the shipping options offered to your customers.<br /><br />');
		echo('The premium version gives you many benefits');
		echo('<ul>');
		echo('<li>1. Re-Name Shipping Methods</li>');
		echo('<li>2. Add Handling Fees</li>');
		echo('<li>3. Enable or Disable Shipping Methods</li>');
		echo('<li>4. Define Shipping Boxes To Use</li>');
		echo('<li>5. Ship By Dimensions and Weight</li>');
		echo('</ul>');
		echo('<br />');
		echo('<a href="http://www.nositesleft.com">Check out our website to see all of the premium version features.</a>');
		echo('<br /><br />');
		echo('<a href="http://www.nositesleft.com"><img src="'. plugin_dir_url(__FILE__) .'premium_features.png" /></a>');
		echo('</p>');
	}
}

?>